package by.bsuir.ivan_bondarau.vitaju.runner;

import by.bsuir.ivan_bondarau.vitaju.model.UserRole;
import by.bsuir.ivan_bondarau.vitaju.repository.UserRoleRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoadRunner implements ApplicationRunner {

    private final UserRoleRepository roleRepository;

    public DataLoadRunner(UserRoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public void run(ApplicationArguments args) {

        UserRole userRole = new UserRole("USER");
        UserRole adminRole = new UserRole("ADMIN");
        UserRole anonRole = new UserRole("ANONYMOUS");

        roleRepository.save(userRole);
        roleRepository.save(adminRole);
        roleRepository.save(anonRole);
    }

}
