package by.bsuir.ivan_bondarau.vitaju.service;

import by.bsuir.ivan_bondarau.vitaju.dto.ChangePasswordDto;
import by.bsuir.ivan_bondarau.vitaju.dto.FillBalanceDto;
import by.bsuir.ivan_bondarau.vitaju.dto.UserDto;

public interface UserService extends CrudService<UserDto> {

    UserDto getUserByUsername(String username);

    void changePassword(UserDto userDto, ChangePasswordDto changePasswordDto);

    void fillWallet(FillBalanceDto balanceDto);
}
