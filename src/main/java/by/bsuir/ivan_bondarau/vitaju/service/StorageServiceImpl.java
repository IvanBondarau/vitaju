package by.bsuir.ivan_bondarau.vitaju.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

@Service
public class StorageServiceImpl implements StorageService {
    private static final Logger LOGGER = LoggerFactory.getLogger(StorageServiceImpl.class);

    private static final String MEDIA_DIR = "~/vitaju-efs/media/";

    private final ResourceLoader resourceLoader;

    @Autowired
    public StorageServiceImpl(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @Override
    public void save(MultipartFile file, HttpServletRequest request) {
        if (file == null) {
            LOGGER.error("HELL");
            return;
        }
        if (!file.isEmpty()) {
            try {
                String orgName = file.getOriginalFilename();
                String filePath = MEDIA_DIR + orgName;
                LOGGER.info(filePath);
                File dest = new File(filePath);
                if (!dest.exists()) {
                    dest.createNewFile();
                }
                file.transferTo(dest);
            } catch (IOException e) {
                throw new RuntimeException("Unable to save file", e);
            }
        }
    }

    @Override
    public File load(String filename) {
        File file = new File(MEDIA_DIR + filename);
        if (file.exists()) {
            return file;
        } else {
            throw new RuntimeException("File " + filename + "not found");
        }

    }

    @Override
    public Resource loadAsResource(String filename) {
        return resourceLoader.getResource("file:/" + MEDIA_DIR + filename);
    }
}
