package by.bsuir.ivan_bondarau.vitaju.service;

import by.bsuir.ivan_bondarau.vitaju.converter.Converter;
import by.bsuir.ivan_bondarau.vitaju.dto.ChangePasswordDto;
import by.bsuir.ivan_bondarau.vitaju.dto.FillBalanceDto;
import by.bsuir.ivan_bondarau.vitaju.dto.UserDto;
import by.bsuir.ivan_bondarau.vitaju.exception.CredentialsAlreadyExistException;
import by.bsuir.ivan_bondarau.vitaju.exception.RoleNotFoundException;
import by.bsuir.ivan_bondarau.vitaju.exception.UserNotFoundException;
import by.bsuir.ivan_bondarau.vitaju.model.UserAccount;
import by.bsuir.ivan_bondarau.vitaju.model.UserRole;
import by.bsuir.ivan_bondarau.vitaju.model.UserRoleName;
import by.bsuir.ivan_bondarau.vitaju.model.UserWallet;
import by.bsuir.ivan_bondarau.vitaju.repository.UserAccountRepository;
import by.bsuir.ivan_bondarau.vitaju.repository.UserPersonalRepository;
import by.bsuir.ivan_bondarau.vitaju.repository.UserRoleRepository;
import by.bsuir.ivan_bondarau.vitaju.repository.UserWalletRepository;
import by.bsuir.ivan_bondarau.vitaju.util.Utils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserAccountRepository userAccountRepository;
    private final UserPersonalRepository userPersonalRepository;
    private final UserWalletRepository userWalletRepository;
    private final UserRoleRepository userRoleRepository;
    private final Converter<UserAccount, UserDto> userConverter;

    public UserServiceImpl(UserAccountRepository userAccountRepository, UserPersonalRepository userPersonalRepository, UserWalletRepository userWalletRepository, UserRoleRepository userRoleRepository, Converter<UserAccount, UserDto> userConverter) {
        this.userAccountRepository = userAccountRepository;
        this.userPersonalRepository = userPersonalRepository;
        this.userWalletRepository = userWalletRepository;
        this.userRoleRepository = userRoleRepository;
        this.userConverter = userConverter;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public UserDto create(UserDto dto) {
        UserAccount entity = userConverter.convertToEntity(dto);
        validateCredentials(entity);
        entity = saveUser(entity);
        return userConverter.convertToDto(entity);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public UserDto read(Long id) {
        UserAccount entity = userAccountRepository.getOne(id);
        return userConverter.convertToDto(entity);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public UserDto update(UserDto dto) {
        UserAccount entity = userConverter.convertToEntity(dto);
        entity = updateUser(entity);
        return userConverter.convertToDto(entity);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Long id) {
        userAccountRepository.deleteById(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<UserDto> findAll() {
        return userAccountRepository.findAll()
                .stream()
                .map(userConverter::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public UserDto getUserByUsername(String username) {
        return userConverter.convertToDto(
                userAccountRepository
                        .findByUsername(username)
                        .orElseThrow(UserNotFoundException::new)
        );
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void changePassword(UserDto userDto, ChangePasswordDto changePasswordDto) {
        if (!userDto.getPassword().equals(changePasswordDto.getOldPassword())) {
            throw new RuntimeException("Invalid password");
        }

        userDto.setPassword(changePasswordDto.getPassword());
        update(userDto);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void fillWallet(FillBalanceDto balanceDto) {
        UserWallet userWallet = userWalletRepository.getOne(balanceDto.getWalletId());
        userWallet.setBalance(
                userWallet.getBalance().add(balanceDto.getValue())
        );
        userWalletRepository.save(userWallet);
    }

    private UserAccount saveUser(UserAccount userAccount) {

        userPersonalRepository.save(
                userAccount.getUserPersonal()
        );

        userWalletRepository.save(
                userAccount.getUserWallet()
        );

        if (userAccount.getRoles().isEmpty()) {
            userAccount.setRoles(
                    Utils.createSet(getDefaultUserRole())
            );
        }

        return userAccountRepository.save(userAccount);
    }

    private UserRole getDefaultUserRole() {
        return userRoleRepository.findByName(
                UserRoleName.USER.name()
        ).orElseThrow(RoleNotFoundException::new);
    }

    private UserAccount updateUser(UserAccount userAccount) {
        return userAccountRepository.save(userAccount);
    }

    private boolean isEmailExists(String email) {
        return userAccountRepository.existsByEmailAndActive(email, true);
    }

    private boolean isUsernameExists(String username) {
        return userAccountRepository.existsByUsernameAndActive(username, true);
    }

    private void validateCredentials(UserAccount userAccount) {
        if (isEmailExists(userAccount.getEmail())
                || isUsernameExists(userAccount.getUsername())) {
            throw new CredentialsAlreadyExistException();
        }
    }


}
