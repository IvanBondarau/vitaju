package by.bsuir.ivan_bondarau.vitaju.service;

import by.bsuir.ivan_bondarau.vitaju.converter.Converter;
import by.bsuir.ivan_bondarau.vitaju.dto.ReservationDto;
import by.bsuir.ivan_bondarau.vitaju.model.Reservation;
import by.bsuir.ivan_bondarau.vitaju.model.UserWallet;
import by.bsuir.ivan_bondarau.vitaju.repository.ReservationRepository;
import by.bsuir.ivan_bondarau.vitaju.repository.UserWalletRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReservationServiceImpl implements ReservationService {


    private final ReservationRepository reservationRepository;
    private final UserWalletRepository userWalletRepository;

    private final Converter<Reservation, ReservationDto> converter;

    public ReservationServiceImpl(ReservationRepository reservationRepository,
                                  UserWalletRepository userWalletRepository,
                                  Converter<Reservation, ReservationDto> converter) {
        this.reservationRepository = reservationRepository;
        this.userWalletRepository = userWalletRepository;
        this.converter = converter;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public ReservationDto create(ReservationDto dto) {
        processTransaction(converter.convertToEntity(dto));
        return save(dto);
    }

    private synchronized void processTransaction(Reservation reservation) {
        UserWallet source = reservation.getUser().getUserWallet();
        UserWallet destination = reservation.getLodging().getOwner().getUserWallet();
        BigDecimal value = reservation.getCost();
        if (source.getBalance().compareTo(value) < 0) {
            throw new IllegalArgumentException("Not enough money in source");
        }

        destination.setBalance(destination.getBalance().add(value));
        source.setBalance(destination.getBalance().subtract(value));
        userWalletRepository.save(source);
        userWalletRepository.save(destination);
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public ReservationDto read(Long id) {
        Reservation entity = reservationRepository.getOne(id);
        return converter.convertToDto(entity);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public ReservationDto update(ReservationDto dto) {
        return save(dto);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Long id) {
        reservationRepository.deleteById(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<ReservationDto> findAll() {
        return reservationRepository.findAll()
                .stream()
                .map(converter::convertToDto)
                .collect(Collectors.toList());
    }


    private ReservationDto save(ReservationDto dto) {
        Reservation reservation = converter.convertToEntity(dto);
        reservation = reservationRepository.save(reservation);
        return converter.convertToDto(reservation);
    }


}
