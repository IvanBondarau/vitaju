package by.bsuir.ivan_bondarau.vitaju.service;

import by.bsuir.ivan_bondarau.vitaju.converter.LodgingConverter;
import by.bsuir.ivan_bondarau.vitaju.dto.LodgingDto;
import by.bsuir.ivan_bondarau.vitaju.dto.LodgingSearchCriteria;
import by.bsuir.ivan_bondarau.vitaju.exception.FacilityNotFoundException;
import by.bsuir.ivan_bondarau.vitaju.exception.InvalidLodgingOwnerException;
import by.bsuir.ivan_bondarau.vitaju.exception.LodgingNotFoundException;
import by.bsuir.ivan_bondarau.vitaju.model.Facility;
import by.bsuir.ivan_bondarau.vitaju.model.Lodging;
import by.bsuir.ivan_bondarau.vitaju.model.UserAccount;
import by.bsuir.ivan_bondarau.vitaju.repository.FacilityRepository;
import by.bsuir.ivan_bondarau.vitaju.repository.LodgingRepository;
import by.bsuir.ivan_bondarau.vitaju.repository.ReservationRepository;
import by.bsuir.ivan_bondarau.vitaju.repository.UserAccountRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class LodgingServiceImpl implements LodgingService {

    private final LodgingRepository lodgingRepository;
    private final FacilityRepository facilityRepository;
    private final UserAccountRepository userAccountRepository;
    private final ReservationRepository reservationRepository;

    private final LodgingConverter lodgingConverter;

    public LodgingServiceImpl(LodgingRepository lodgingRepository,
                              FacilityRepository facilityRepository,
                              UserAccountRepository userAccountRepository,
                              ReservationRepository reservationRepository,
                              LodgingConverter lodgingConverter) {
        this.lodgingRepository = lodgingRepository;
        this.facilityRepository = facilityRepository;
        this.userAccountRepository = userAccountRepository;
        this.reservationRepository = reservationRepository;
        this.lodgingConverter = lodgingConverter;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public LodgingDto create(LodgingDto dto) {
        Lodging entity = lodgingConverter.convertToEntity(dto);
        entity = saveLodging(entity);
        return lodgingConverter.convertToDto(entity);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public LodgingDto read(Long id) {
        Lodging entity = lodgingRepository.getOne(id);
        return lodgingConverter.convertToDto(entity);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public LodgingDto update(LodgingDto dto) {
        Lodging entity = lodgingConverter.convertToEntity(dto);
        entity = updateLodging(entity);
        return lodgingConverter.convertToDto(entity);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Long id) {
        if (!lodgingRepository.existsById(id)) {
            throw new LodgingNotFoundException("Lodging with id " + id + " not found");
        }
        reservationRepository.deleteByLodgingId(id);
        lodgingRepository.deleteById(id);
    }

    @Override
    public List<LodgingDto> find(LodgingSearchCriteria searchCriteria) {
        List<Lodging> result;
        if (searchCriteria.getName() == null) {
            result = lodgingRepository.findByLocationLike(
                    "%" + searchCriteria.getLocation() + "%");
        } else if (searchCriteria.getLocation() == null) {
            result = lodgingRepository.findByNameLike(
                    "%" + searchCriteria.getName() + "%");
        } else {
            result = lodgingRepository.findByNameLikeAndLocationLike(
                    "%" + searchCriteria.getName() + "%",
                    "%" + searchCriteria.getLocation() + "%");
        }

        return result
                .stream()
                .map(lodgingConverter::convertToDto)
                .collect(Collectors.toList());

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<LodgingDto> findAll() {
        return lodgingRepository
                .findAll()
                .stream()
                .map(lodgingConverter::convertToDto)
                .collect(Collectors.toList());
    }

    private Lodging saveLodging(Lodging lodging) {

        if (lodging.getOwner() == null || lodging.getOwner().getId() == null) {
            throw new InvalidLodgingOwnerException("Lodging owner's id is not specified");
        }

        loadOwner(lodging);
        loadFacilities(lodging);

        return lodgingRepository.save(lodging);
    }

    private Lodging updateLodging(Lodging lodging) {
        return saveLodging(lodging);
    }

    private void loadOwner(Lodging lodging) {
        UserAccount owner = userAccountRepository.getOne(lodging.getOwner().getId());
        lodging.setOwner(owner);
    }

    private void loadFacilities(Lodging lodging) {
        Set<Facility> facilities = lodging.getFacilities();
        for (var facility : facilities) {
            Optional<Facility> searchResult = facilityRepository.findByName(facility.getName());
            facility.setId(
                    searchResult.orElseThrow(FacilityNotFoundException::new).getId()
            );
        }
    }
}
