package by.bsuir.ivan_bondarau.vitaju.service;

import by.bsuir.ivan_bondarau.vitaju.dto.LodgingDto;
import by.bsuir.ivan_bondarau.vitaju.dto.LodgingSearchCriteria;

import java.util.List;

public interface LodgingService extends CrudService<LodgingDto> {

    List<LodgingDto> find(LodgingSearchCriteria searchCriteria);
}
