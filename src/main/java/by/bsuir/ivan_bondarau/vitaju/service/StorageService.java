package by.bsuir.ivan_bondarau.vitaju.service;

import org.springframework.core.io.Resource;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

public interface StorageService {

    void save(MultipartFile file, HttpServletRequest request);
    File load(String filename);
    Resource loadAsResource(String filename);
}
