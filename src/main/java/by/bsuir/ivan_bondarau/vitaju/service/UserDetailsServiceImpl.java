package by.bsuir.ivan_bondarau.vitaju.service;

import by.bsuir.ivan_bondarau.vitaju.model.UserAccount;
import by.bsuir.ivan_bondarau.vitaju.model.UserRole;
import by.bsuir.ivan_bondarau.vitaju.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    private final PasswordEncoder encoder;

    private final UserAccountRepository userAccountRepository;

    @Autowired
    public UserDetailsServiceImpl(PasswordEncoder encoder,
                                  UserAccountRepository userAccountRepository) {
        this.encoder = encoder;
        this.userAccountRepository = userAccountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<UserAccount> searchResult = userAccountRepository.findByUsername(s);

        UserAccount userAccount = searchResult.orElseThrow(() -> new UsernameNotFoundException(s));

        return User.withUsername(userAccount.getUsername())
                .password(encoder.encode(userAccount.getPassword()))
                .roles(
                        userAccount
                                .getRoles()
                                .stream()
                                .map(UserRole::getName)
                                .toArray(String[]::new)
                )
                .build();

    }

    private static final class Authority implements GrantedAuthority {
        private final String name;

        public Authority(UserRole role) {
            this.name = role.getName();
        }

        @Override
        public String getAuthority() {
            return name;
        }
    }
}
