package by.bsuir.ivan_bondarau.vitaju.service;

import java.util.List;

public interface CrudService<T> {

    T create(T dto);

    T read(Long id);

    T update(T dto);

    void delete(Long id);

    List<T> findAll();
}
