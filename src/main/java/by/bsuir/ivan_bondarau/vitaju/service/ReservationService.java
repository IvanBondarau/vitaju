package by.bsuir.ivan_bondarau.vitaju.service;

import by.bsuir.ivan_bondarau.vitaju.dto.ReservationDto;
import by.bsuir.ivan_bondarau.vitaju.dto.ReservationSearchCriteria;

import java.util.List;

public interface ReservationService extends CrudService<ReservationDto> {

}
