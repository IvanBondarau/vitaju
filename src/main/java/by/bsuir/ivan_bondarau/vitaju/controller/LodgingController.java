package by.bsuir.ivan_bondarau.vitaju.controller;

import by.bsuir.ivan_bondarau.vitaju.dto.LodgingDto;
import by.bsuir.ivan_bondarau.vitaju.dto.LodgingSearchCriteria;
import by.bsuir.ivan_bondarau.vitaju.dto.ReservationDto;
import by.bsuir.ivan_bondarau.vitaju.dto.UserDto;
import by.bsuir.ivan_bondarau.vitaju.service.LodgingService;
import by.bsuir.ivan_bondarau.vitaju.service.StorageService;
import by.bsuir.ivan_bondarau.vitaju.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping(value = "/lodging")
public class LodgingController {


    private Logger LOGGER = LoggerFactory.getLogger(LodgingController.class);

    private LodgingService lodgingService;
    private UserService userService;
    private StorageService storageService;

    @Autowired
    private HttpServletRequest request;

    public LodgingController(LodgingService lodgingService, UserService userService, StorageService storageService) {
        this.lodgingService = lodgingService;
        this.userService = userService;
        this.storageService = storageService;
    }

    @GetMapping
    public String getAllLodging(Model model) {
        List<LodgingDto> lodgingList = lodgingService.findAll();
        model.addAttribute("lodgingList", lodgingList);
        model.addAttribute("searchCriteria", new LodgingSearchCriteria());
        return "lodging-list";
    }

    @PostMapping(value = "/search")
    public String searchLodging(
            @ModelAttribute(name = "searchCriteria") LodgingSearchCriteria searchCriteria,
            Model model) {
        LOGGER.info(searchCriteria.toString());
        List<LodgingDto> lodgingDtoList = lodgingService.find(searchCriteria);
        LOGGER.info(String.valueOf(lodgingDtoList.size()));
        model.addAttribute("lodgingList", lodgingDtoList);
        model.addAttribute("searchCriteria", new LodgingSearchCriteria());
        return "lodging-list";
    }

    @GetMapping(value = "/{id}")
    public String getLodging(Model model, @PathVariable Long id) {
        LodgingDto lodging = lodgingService.read(id);
        model.addAttribute("lodging", lodging);
        model.addAttribute("reservationPrepare", new ReservationDto());
        return "lodging-page";
    }

    @GetMapping(value = "/create")
    public String getCreatePage(Model model, Principal principal) {
        UserDto userDto = userService.getUserByUsername(principal.getName());
        LodgingDto lodgingDto = new LodgingDto();
        model.addAttribute("currentUser", userDto);
        model.addAttribute("lodging", lodgingDto);
        return "lodging-create";

    }

    @PostMapping
    public View create(@ModelAttribute(name = "lodging") @Valid LodgingDto lodgingDto) {
        LOGGER.info(lodgingDto.toString());

        storageService.save(lodgingDto.getPhoto(), request);
        lodgingDto = lodgingService.create(lodgingDto);

        return new RedirectView("/lodging/" + lodgingDto.getId());
    }

    @GetMapping(value = "/{id}/delete")
    public RedirectView delete(@PathVariable Long id) {
        lodgingService.delete(id);
        return new RedirectView("/lodging");
    }
}
