package by.bsuir.ivan_bondarau.vitaju.controller;

import by.bsuir.ivan_bondarau.vitaju.dto.UserDto;
import by.bsuir.ivan_bondarau.vitaju.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    private UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/register")
    public String getRegisterPage(Model model) {
        UserDto userDto = new UserDto();
        model.addAttribute("user", userDto);
        return "registration";
    }

    @PostMapping(value = "/register")
    public View register(@ModelAttribute(name = "user") @Valid UserDto userDto) {
        userService.create(userDto);
        return new RedirectView("/user");
    }

}
