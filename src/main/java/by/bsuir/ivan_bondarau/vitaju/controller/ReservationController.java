package by.bsuir.ivan_bondarau.vitaju.controller;


import by.bsuir.ivan_bondarau.vitaju.converter.ReservationConverter;
import by.bsuir.ivan_bondarau.vitaju.dto.LodgingDto;
import by.bsuir.ivan_bondarau.vitaju.dto.ReservationDto;
import by.bsuir.ivan_bondarau.vitaju.dto.UserDto;
import by.bsuir.ivan_bondarau.vitaju.exception.DateFormatException;
import by.bsuir.ivan_bondarau.vitaju.service.LodgingService;
import by.bsuir.ivan_bondarau.vitaju.service.ReservationService;
import by.bsuir.ivan_bondarau.vitaju.service.UserService;
import by.bsuir.ivan_bondarau.vitaju.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.math.BigDecimal;
import java.security.Principal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/reservation")
@SessionAttributes("reservation")
public class ReservationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationController.class);

    private ReservationService reservationService;
    private UserService userService;
    private LodgingService lodgingService;

    @Autowired
    public ReservationController(ReservationService reservationService,
                                 UserService userService,
                                 LodgingService lodgingService) {
        this.reservationService = reservationService;
        this.userService = userService;
        this.lodgingService = lodgingService;
    }

    @GetMapping
    public String findReservation(Model model) {

        List<ReservationDto> reservations = reservationService.findAll();
        model.addAttribute("reservationList", reservations);

        return "reservation-list";
    }

    @PostMapping(value = "/create")
    public String getCreateReservationPage(@ModelAttribute(value = "reservationPrepare") ReservationDto reservationDto,
                                           Model model,
                                           Principal principal,
                                           @RequestParam(name = "lodgingId") Long lodgingId) {

        UserDto currentUser = userService.getUserByUsername(principal.getName());
        LodgingDto lodging = lodgingService.read(lodgingId);

        countCost(reservationDto, lodging);
        setUser(reservationDto, currentUser);
        setLodging(reservationDto, lodging);

        LOGGER.info(reservationDto.toString());

        model.addAttribute("reservation", reservationDto);
        return "reservation-create";
    }

    @PostMapping
    public View createReservation(@ModelAttribute(name = "reservation") ReservationDto reservation,
                                  Model model,
                                  RedirectAttributes redirectAttributes) {
        LOGGER.info(reservation.toString());

        reservationService.create(reservation);
        model.addAttribute("reservation", null);
        return new RedirectView("/reservation");
    }


    private void countCost(ReservationDto reservationDto, LodgingDto lodgingDto) {
        Date startDate;
        Date endDate;
        try {
            LOGGER.info(reservationDto.getStartDate());
            startDate = ReservationConverter.DATE_FORMAT.parse(reservationDto.getStartDate());
            endDate = ReservationConverter.DATE_FORMAT.parse(reservationDto.getEndDate());
        } catch (ParseException parse) {
            throw new DateFormatException();
        }
        long duration = Utils.convertToDays(endDate.getTime() - startDate.getTime()) + 1;
        reservationDto.setCost(lodgingDto.getCost().multiply(new BigDecimal(duration)));
    }

    private void setUser(ReservationDto reservationDto, UserDto userDto) {
        reservationDto.setUserId(userDto.getId());
        reservationDto.setUserUsername(userDto.getUsername());
    }

    private void setLodging(ReservationDto reservationDto, LodgingDto lodging) {
        reservationDto.setLodgingId(lodging.getId());
        reservationDto.setLodgingName(lodging.getName());
        reservationDto.setLodgingOwnerId(lodging.getOwnerId());
        reservationDto.setLodgingOwnerUsername(lodging.getOwnerUsername());
    }

    @ModelAttribute(name = "reservation")
    public ReservationDto reservationDto() {
        return null;
    }

}
