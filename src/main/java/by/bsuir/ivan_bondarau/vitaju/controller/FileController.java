package by.bsuir.ivan_bondarau.vitaju.controller;

import by.bsuir.ivan_bondarau.vitaju.service.StorageService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping(value = "/file")
public class FileController {

    @Autowired
    private StorageService storageService;

    @GetMapping("/{filename:.+}")
    @ResponseBody
    public void serveFile(@PathVariable String filename, HttpServletResponse response) throws IOException {

        Resource file = storageService.loadAsResource(filename);
        response.setContentType("image/jpg");
        IOUtils.copy(file.getInputStream(), response.getOutputStream());
    }
}
