package by.bsuir.ivan_bondarau.vitaju.controller;

import by.bsuir.ivan_bondarau.vitaju.dto.ChangePasswordDto;
import by.bsuir.ivan_bondarau.vitaju.dto.FillBalanceDto;
import by.bsuir.ivan_bondarau.vitaju.dto.UserDto;
import by.bsuir.ivan_bondarau.vitaju.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping(value = "/user")
@SessionAttributes("user")
public class UserController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String getCurrentUser(Principal principal, Model model) {
        UserDto user = userService.getUserByUsername(principal.getName());
        model.addAttribute("user", user);
        return "user";
    }

    @GetMapping(value = "/password")
    public String getRegisterPage(Model model) {
        ChangePasswordDto changePasswordDto = new ChangePasswordDto();
        model.addAttribute("passwordDto", changePasswordDto);
        return "password";
    }

    @PostMapping
    public RedirectView registrationPostProcessing() {
        logger.info("POST");
        return new RedirectView("/user");
    }


    @PutMapping
    public RedirectView updateView(@Valid @ModelAttribute("user") UserDto user, Model model) {
        user = userService.update(user);
        model.addAttribute("user", user);
        return new RedirectView("/user");
    }

    @PostMapping(value = "/password")
    public RedirectView updatePassword(
            Principal principal,
            @Valid ChangePasswordDto changePasswordDto,
            HttpSession httpSession) {
        UserDto user = userService.getUserByUsername(principal.getName());
        userService.changePassword(user, changePasswordDto);
        httpSession.invalidate();
        SecurityContextHolder.clearContext();
        return new RedirectView("/login");
    }

    @GetMapping("/wallet/fill")
    public String getFillWalletPage(Model model) {
        FillBalanceDto fillBalanceDto = new FillBalanceDto();
        model.addAttribute("balance", fillBalanceDto);
        return "fill-wallet";
    }

    @PostMapping(value = "/wallet")
    public RedirectView fillWallet(@ModelAttribute(name = "balance") FillBalanceDto balanceDto,
                                   Principal principal) {
        UserDto currentUser = userService.getUserByUsername(principal.getName());
        balanceDto.setWalletId(currentUser.getUserWalletId());
        userService.fillWallet(balanceDto);
        return new RedirectView("/user");
    }

    @GetMapping(value = "/all")
    public String getAllUsers(Model model) {
        model.addAttribute("users", userService.findAll());
        return "user-list";
    }

}
