package by.bsuir.ivan_bondarau.vitaju.exception;

public class LodgingNotFoundException extends RuntimeException {

    public LodgingNotFoundException() {
    }

    public LodgingNotFoundException(String message) {
        super(message);
    }

    public LodgingNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public LodgingNotFoundException(Throwable cause) {
        super(cause);
    }
}
