package by.bsuir.ivan_bondarau.vitaju.exception;

public class InvalidLodgingOwnerException extends RuntimeException {
    public InvalidLodgingOwnerException() {
    }

    public InvalidLodgingOwnerException(String message) {
        super(message);
    }

    public InvalidLodgingOwnerException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidLodgingOwnerException(Throwable cause) {
        super(cause);
    }
}
