package by.bsuir.ivan_bondarau.vitaju.exception;

public class DateFormatException extends RuntimeException {
    public DateFormatException() {
    }

    public DateFormatException(String message) {
        super(message);
    }

    public DateFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public DateFormatException(Throwable cause) {
        super(cause);
    }
}
