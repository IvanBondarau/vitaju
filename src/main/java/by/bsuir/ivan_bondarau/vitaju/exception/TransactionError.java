package by.bsuir.ivan_bondarau.vitaju.exception;

public class TransactionError extends RuntimeException {

    public TransactionError() {
    }

    public TransactionError(String message) {
        super(message);
    }

    public TransactionError(String message, Throwable cause) {
        super(message, cause);
    }

    public TransactionError(Throwable cause) {
        super(cause);
    }
}
