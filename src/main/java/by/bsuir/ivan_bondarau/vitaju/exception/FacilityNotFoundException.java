package by.bsuir.ivan_bondarau.vitaju.exception;

public class FacilityNotFoundException extends RuntimeException {
    public FacilityNotFoundException() {
    }

    public FacilityNotFoundException(String message) {
        super(message);
    }

    public FacilityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public FacilityNotFoundException(Throwable cause) {
        super(cause);
    }
}
