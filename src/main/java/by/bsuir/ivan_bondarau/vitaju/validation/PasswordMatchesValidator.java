package by.bsuir.ivan_bondarau.vitaju.validation;

import by.bsuir.ivan_bondarau.vitaju.dto.ChangePasswordDto;
import by.bsuir.ivan_bondarau.vitaju.dto.UserDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(final PasswordMatches constraintAnnotation) {
        //
    }

    @Override
    public boolean isValid(final Object obj, final ConstraintValidatorContext context) {

        if (obj instanceof UserDto) {
            final UserDto user = (UserDto) obj;
            return user.getPasswordConfirmation() == null
                    || user.getPassword().equals(user.getPasswordConfirmation());
        } else {
            final ChangePasswordDto changePasswordDto = (ChangePasswordDto) obj;
            return changePasswordDto.getPasswordConfirmation() == null
                    || changePasswordDto.getPassword().equals(changePasswordDto.getPasswordConfirmation());
        }

    }

}