package by.bsuir.ivan_bondarau.vitaju.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "facility")
public class Facility extends IdentifiedEntity {
    @Column(name="name")
    private String name;

    public Facility() {
    }

    public Facility(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Facility facility = (Facility) o;
        return Objects.equals(name, facility.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name);
    }

    @Override
    public String toString() {
        return "Facility{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}
