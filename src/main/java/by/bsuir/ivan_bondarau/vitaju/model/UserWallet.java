package by.bsuir.ivan_bondarau.vitaju.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "user_wallet")
public class UserWallet extends IdentifiedEntity {

    @Column(name = "balance")
    private BigDecimal balance;

    @OneToOne(mappedBy = "userWallet")
    private UserAccount userAccount;

    public UserWallet() {
        this.balance = BigDecimal.ZERO;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserWallet that = (UserWallet) o;
        return Objects.equals(balance, that.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), balance);
    }

    @Override
    public String toString() {
        return "UserWallet{" +
                "balance=" + balance +
                ", id=" + id +
                '}';
    }
}
