package by.bsuir.ivan_bondarau.vitaju.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "user_role")
public class UserRole extends IdentifiedEntity {

    @Column(name = "name")
    private String name;
    @ManyToMany(mappedBy = "roles")
    private Set<UserAccount> userAccount;

    public UserRole() {

    }

    public UserRole(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserRole userRole = (UserRole) o;
        return Objects.equals(name, userRole.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name);
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}
