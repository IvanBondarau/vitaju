package by.bsuir.ivan_bondarau.vitaju.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "lodging")
public class Lodging extends IdentifiedEntity {
    @OneToOne
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    private UserAccount owner;
    @Column(name = "name")
    private String name;
    @Column(name = "location")
    private String location;
    @Column(name = "description")
    private String description;
    @Column(name = "cost")
    private BigDecimal cost;
    @Column(name = "image_name")
    private String imageName;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "lodging_facilities",
            joinColumns = @JoinColumn(name = "lodging_id"),
            inverseJoinColumns = @JoinColumn(name = "facility_id"))
    private Set<Facility> facilities;

    public UserAccount getOwner() {
        return owner;
    }

    public void setOwner(UserAccount owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String photoName) {
        this.imageName = photoName;
    }

    public Set<Facility> getFacilities() {
        return facilities;
    }

    public void setFacilities(Set<Facility> facilities) {
        this.facilities = facilities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Lodging lodging = (Lodging) o;
        return Objects.equals(owner, lodging.owner) &&
                Objects.equals(name, lodging.name) &&
                Objects.equals(location, lodging.location) &&
                Objects.equals(description, lodging.description) &&
                Objects.equals(cost, lodging.cost) &&
                Objects.equals(facilities, lodging.facilities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), owner, name, location, description, cost, facilities);
    }

    @Override
    public String toString() {
        return "Lodging{" +
                "owner=" + owner +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                ", cost=" + cost +
                ", imageName='" + imageName + '\'' +
                ", facilities=" + facilities +
                '}';
    }
}
