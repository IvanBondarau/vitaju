package by.bsuir.ivan_bondarau.vitaju.model;

import javax.persistence.*;
import java.util.Objects;

@MappedSuperclass
public abstract class IdentifiedEntity {

    @Id
    @SequenceGenerator(name = "vitaju_generator", sequenceName = "vitaju_sequence", allocationSize = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vitaju_generator")
    @Column(name = "id")
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdentifiedEntity that = (IdentifiedEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
