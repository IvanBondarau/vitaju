package by.bsuir.ivan_bondarau.vitaju.model;

public enum UserRoleName {
    ADMIN,
    USER,
    ANONYMOUS,
}
