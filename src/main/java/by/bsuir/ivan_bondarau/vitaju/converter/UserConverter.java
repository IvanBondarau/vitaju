package by.bsuir.ivan_bondarau.vitaju.converter;

import by.bsuir.ivan_bondarau.vitaju.dto.UserDto;
import by.bsuir.ivan_bondarau.vitaju.exception.RoleNotFoundException;
import by.bsuir.ivan_bondarau.vitaju.model.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserConverter implements Converter<UserAccount, UserDto> {

    private ModelMapper modelMapper;
    private UserRoleConverter userRoleConverter;

    @Autowired
    public UserConverter(ModelMapper modelMapper,
                         UserRoleConverter userRoleConverter) {
        this.modelMapper = modelMapper;
        this.userRoleConverter = userRoleConverter;
    }

    @Override
    public UserAccount convertToEntity(UserDto dto) {

        UserAccount userAccount = modelMapper.map(dto, UserAccount.class);

        Set<UserRole> roles = null;
        if (dto.getRoles() != null) {
            roles = dto.getRoles()
                    .stream()
                    .map(userRoleConverter::convertToEntity)
                    .collect(Collectors.toSet());
        } else {
            roles = new HashSet<>();
        }
        userAccount.setRoles(roles);

        UserPersonal userPersonal = new UserPersonal();
        userPersonal.setId(dto.getUserPersonalId());
        userPersonal.setContactEmail(dto.getContactEmail());
        userPersonal.setFullName(dto.getFullName());
        userPersonal.setPhone(dto.getPhone());
        userPersonal.setLocation(dto.getLocation());
        userAccount.setUserPersonal(userPersonal);

        UserWallet userWallet = new UserWallet();
        userWallet.setId(dto.getUserWalletId());
        if (dto.getBalance() == null) {
            userWallet.setBalance(BigDecimal.ZERO);
        } else {
            userWallet.setBalance(dto.getBalance());
        }
        userAccount.setUserWallet(userWallet);

        return userAccount;
    }

    @Override
    public UserDto convertToDto(UserAccount entity) {
        UserDto userDto = modelMapper.map(entity, UserDto.class);

        userDto.setRoles(
                entity.getRoles()
                        .stream()
                        .map(userRoleConverter::convertToDto)
                        .collect(Collectors.toSet())
        );

        if (entity.getUserPersonal() != null) {
            userDto.setUserPersonalId(entity.getUserPersonal().getId());
            userDto.setFullName(entity.getUserPersonal().getFullName());
            userDto.setPhone(entity.getUserPersonal().getPhone());
            userDto.setContactEmail(entity.getUserPersonal().getContactEmail());
            userDto.setLocation(entity.getUserPersonal().getLocation());
        }
        if (entity.getUserWallet() != null) {
            userDto.setUserWalletId(userDto.getUserWalletId());
            userDto.setBalance(entity.getUserWallet().getBalance());
        }

        return userDto;
    }


}
