package by.bsuir.ivan_bondarau.vitaju.converter;

import by.bsuir.ivan_bondarau.vitaju.exception.RoleNotFoundException;
import by.bsuir.ivan_bondarau.vitaju.model.UserRole;
import by.bsuir.ivan_bondarau.vitaju.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserRoleConverter implements Converter<UserRole, String> {

    private UserRoleRepository userRoleRepository;

    @Autowired
    public UserRoleConverter(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public UserRole convertToEntity(String name) {
        return userRoleRepository
                .findByName(name)
                .orElseThrow(RoleNotFoundException::new);
    }

    @Override
    public String convertToDto(UserRole entity) {
        return entity.getName();
    }
}
