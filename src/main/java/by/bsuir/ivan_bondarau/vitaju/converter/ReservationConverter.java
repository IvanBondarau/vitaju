package by.bsuir.ivan_bondarau.vitaju.converter;

import by.bsuir.ivan_bondarau.vitaju.dto.ReservationDto;
import by.bsuir.ivan_bondarau.vitaju.exception.DateFormatException;
import by.bsuir.ivan_bondarau.vitaju.model.Lodging;
import by.bsuir.ivan_bondarau.vitaju.model.Reservation;
import by.bsuir.ivan_bondarau.vitaju.model.UserAccount;
import by.bsuir.ivan_bondarau.vitaju.repository.LodgingRepository;
import by.bsuir.ivan_bondarau.vitaju.repository.UserAccountRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Component
public class ReservationConverter implements Converter<Reservation, ReservationDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationConverter.class);
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");


    private ModelMapper modelMapper;

    private UserAccountRepository userAccountRepository;
    private LodgingRepository lodgingRepository;


    public ReservationConverter(ModelMapper modelMapper,
                                UserAccountRepository userAccountRepository,
                                LodgingRepository lodgingRepository) {
        this.modelMapper = modelMapper;
        this.userAccountRepository = userAccountRepository;
        this.lodgingRepository = lodgingRepository;
    }

    @Override
    public Reservation convertToEntity(ReservationDto dto) {
        Reservation entity = modelMapper.map(dto, Reservation.class);
        try {
            entity.setStartDate(DATE_FORMAT.parse(dto.getStartDate()));
            entity.setEndDate(DATE_FORMAT.parse(dto.getEndDate()));
        } catch (ParseException e) {
            LOGGER.error("Invalid date format: " + dto.toString());
            throw new DateFormatException();
        }


        Long userId = dto.getUserId();
        UserAccount user = userAccountRepository.getOne(userId);
        entity.setUser(user);

        Long lodgingId = dto.getLodgingId();
        Lodging lodging = lodgingRepository.getOne(lodgingId);
        entity.setLodging(lodging);

        return entity;
    }

    @Override
    public ReservationDto convertToDto(Reservation entity) {
        ReservationDto reservationDto = modelMapper.map(entity, ReservationDto.class);
        reservationDto.setStartDate(DATE_FORMAT.format(entity.getStartDate()));
        reservationDto.setEndDate(DATE_FORMAT.format(entity.getEndDate()));
        return reservationDto;
    }
}
