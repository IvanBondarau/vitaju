package by.bsuir.ivan_bondarau.vitaju.converter;

public interface Converter<E, D> {
    E convertToEntity(D dto);
    D convertToDto(E entity);
}
