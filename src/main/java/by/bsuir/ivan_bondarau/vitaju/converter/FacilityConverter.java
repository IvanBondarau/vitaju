package by.bsuir.ivan_bondarau.vitaju.converter;

import by.bsuir.ivan_bondarau.vitaju.exception.FacilityNotFoundException;
import by.bsuir.ivan_bondarau.vitaju.model.Facility;
import by.bsuir.ivan_bondarau.vitaju.repository.FacilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FacilityConverter implements Converter<Facility, String> {

    private FacilityRepository facilityRepository;

    @Autowired
    public FacilityConverter(FacilityRepository facilityRepository) {
        this.facilityRepository = facilityRepository;
    }

    @Override
    public Facility convertToEntity(String name) {
        return facilityRepository
                .findByName(name)
                .orElseThrow(FacilityNotFoundException::new);
    }

    @Override
    public String convertToDto(Facility entity) {
        return entity.getName();
    }
}
