package by.bsuir.ivan_bondarau.vitaju.converter;

import by.bsuir.ivan_bondarau.vitaju.dto.LodgingDto;
import by.bsuir.ivan_bondarau.vitaju.model.Lodging;
import by.bsuir.ivan_bondarau.vitaju.model.UserAccount;
import by.bsuir.ivan_bondarau.vitaju.repository.UserAccountRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class LodgingConverter implements Converter<Lodging, LodgingDto> {

    private static final String DEFAULT_PHOTO_NAME = "default-image.jpg";

    private ModelMapper modelMapper;

    private UserAccountRepository userAccountRepository;

    private FacilityConverter facilityConverter;

    @Autowired
    public LodgingConverter(ModelMapper modelMapper,
                            UserAccountRepository userAccountRepository,
                            FacilityConverter facilityConverter) {
        this.modelMapper = modelMapper;
        this.userAccountRepository = userAccountRepository;
        this.facilityConverter = facilityConverter;
    }

    @Override
    public Lodging convertToEntity(LodgingDto dto) {
        Lodging entity = modelMapper.map(dto, Lodging.class);

        if (dto.getPhoto() != null) {
            entity.setImageName(dto.getPhoto().getOriginalFilename());
        } else {
            if (dto.getPhotoName() == null) {
                entity.setImageName(DEFAULT_PHOTO_NAME);
            } else {
                entity.setImageName(dto.getPhotoName());
            }
        }

        entity.setFacilities(
                dto.getFacilities()
                        .stream()
                        .map(facilityConverter::convertToEntity)
                        .collect(Collectors.toSet())
        );

        Long ownerId = dto.getOwnerId();
        UserAccount owner = userAccountRepository.getOne(ownerId);
        entity.setOwner(owner);

        return entity;
    }

    @Override
    public LodgingDto convertToDto(Lodging entity) {
        LodgingDto dto = modelMapper.map(entity, LodgingDto.class);

        dto.setPhotoName(entity.getImageName());

        dto.setFacilities(
                entity.getFacilities()
                        .stream()
                        .map(facilityConverter::convertToDto)
                        .collect(Collectors.toSet())
        );

        return dto;
    }
}
