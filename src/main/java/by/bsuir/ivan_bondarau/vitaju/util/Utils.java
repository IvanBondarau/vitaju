package by.bsuir.ivan_bondarau.vitaju.util;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Utils {

    @SafeVarargs
    public static <T> Set<T> createSet(T... args) {
        return new HashSet<>(Arrays.asList(args));
    }

    public static long convertToDays(long milliseconds) {
        return milliseconds / 1000 / 60 / 60 / 24;
    }


}
