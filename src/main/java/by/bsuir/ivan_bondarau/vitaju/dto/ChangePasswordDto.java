package by.bsuir.ivan_bondarau.vitaju.dto;


import by.bsuir.ivan_bondarau.vitaju.validation.PasswordMatches;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@PasswordMatches
public class ChangePasswordDto {

    @NotNull
    @NotEmpty
    @Length(min = 5, max = 30)
    private String oldPassword;

    @NotNull
    @NotEmpty
    @Length(min = 5, max = 30)
    private String password;

    private String passwordConfirmation;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
