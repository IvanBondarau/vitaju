package by.bsuir.ivan_bondarau.vitaju.dto;

import java.math.BigDecimal;
import java.util.Objects;

public class FillBalanceDto {
    private Long walletId;
    private BigDecimal value;

    public Long getWalletId() {
        return walletId;
    }

    public void setWalletId(Long walletId) {
        this.walletId = walletId;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FillBalanceDto that = (FillBalanceDto) o;
        return Objects.equals(walletId, that.walletId) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(walletId, value);
    }
}
