package by.bsuir.ivan_bondarau.vitaju.dto;

import java.util.Objects;

public class ReservationSearchCriteria {

    private Long userId;
    private Long lodgingId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getLodgingId() {
        return lodgingId;
    }

    public void setLodgingId(Long lodgingId) {
        this.lodgingId = lodgingId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReservationSearchCriteria that = (ReservationSearchCriteria) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(lodgingId, that.lodgingId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, lodgingId);
    }

    @Override
    public String toString() {
        return "ReservationSearchCriteria{" +
                "userId=" + userId +
                ", lodgingId=" + lodgingId +
                '}';
    }
}
