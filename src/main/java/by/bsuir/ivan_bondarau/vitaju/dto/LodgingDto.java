package by.bsuir.ivan_bondarau.vitaju.dto;

import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class LodgingDto {

    private Long id;
    @NotNull
    private Long ownerId;

    @NotNull
    private String ownerUsername;

    @NotNull
    @Length(max = 50)
    private String name;
    @NotNull
    @Length(max = 300)
    private String location;
    @NotNull
    @Length(max = 2000)
    private String description;
    @NotNull
    @Positive
    private BigDecimal cost;
    @NotNull
    private Set<String> facilities;

    private MultipartFile photo;

    private String photoName;

    public LodgingDto() {
        this.ownerId = Long.MIN_VALUE;
        this.name = "";
        this.ownerUsername = "";
        this.location = "";
        this.description = "";
        this.cost = BigDecimal.ZERO;
        this.facilities = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerUsername() {
        return ownerUsername;
    }

    public void setOwnerUsername(String ownerUsername) {
        this.ownerUsername = ownerUsername;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Set<String> getFacilities() {
        return facilities;
    }

    public void setFacilities(Set<String> facilities) {
        this.facilities = facilities;
    }

    public MultipartFile getPhoto() {
        return photo;
    }

    public void setPhoto(MultipartFile photo) {
        this.photo = photo;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LodgingDto dto = (LodgingDto) o;
        return Objects.equals(id, dto.id) &&
                Objects.equals(ownerId, dto.ownerId) &&
                Objects.equals(ownerUsername, dto.ownerUsername) &&
                Objects.equals(name, dto.name) &&
                Objects.equals(location, dto.location) &&
                Objects.equals(description, dto.description) &&
                Objects.equals(cost, dto.cost) &&
                Objects.equals(facilities, dto.facilities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ownerId, ownerUsername, name, location, description, cost, facilities);
    }

    @Override
    public String toString() {
        return "LodgingDto{" +
                "id=" + id +
                ", ownerId=" + ownerId +
                ", ownerUsername='" + ownerUsername + '\'' +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                ", cost=" + cost +
                ", facilities=" + facilities +
                ", lodgingPhoto=" + photo +
                '}';
    }
}
