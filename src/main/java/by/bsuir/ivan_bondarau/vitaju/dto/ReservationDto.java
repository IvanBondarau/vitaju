package by.bsuir.ivan_bondarau.vitaju.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Objects;

public class ReservationDto {

    private Long id;

    private BigDecimal cost;

    @NotNull
    private String startDate;

    @NotNull
    private String endDate;

    private String comment;

    private Long userId;
    private String userUsername;

    private Long lodgingOwnerId;
    private String lodgingOwnerUsername;

    private Long lodgingId;
    private String lodgingName;

    public ReservationDto() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserUsername() {
        return userUsername;
    }

    public void setUserUsername(String userUsername) {
        this.userUsername = userUsername;
    }

    public Long getLodgingOwnerId() {
        return lodgingOwnerId;
    }

    public void setLodgingOwnerId(Long lodgingOwnerId) {
        this.lodgingOwnerId = lodgingOwnerId;
    }

    public String getLodgingOwnerUsername() {
        return lodgingOwnerUsername;
    }

    public void setLodgingOwnerUsername(String lodgingOwnerUsername) {
        this.lodgingOwnerUsername = lodgingOwnerUsername;
    }

    public Long getLodgingId() {
        return lodgingId;
    }

    public void setLodgingId(Long lodgingId) {
        this.lodgingId = lodgingId;
    }

    public String getLodgingName() {
        return lodgingName;
    }

    public void setLodgingName(String lodgingName) {
        this.lodgingName = lodgingName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReservationDto that = (ReservationDto) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(cost, that.cost) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(userUsername, that.userUsername) &&
                Objects.equals(lodgingOwnerId, that.lodgingOwnerId) &&
                Objects.equals(lodgingOwnerUsername, that.lodgingOwnerUsername) &&
                Objects.equals(lodgingId, that.lodgingId) &&
                Objects.equals(lodgingName, that.lodgingName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cost, startDate, endDate, comment, userId, userUsername, lodgingOwnerId, lodgingOwnerUsername, lodgingId, lodgingName);
    }

    @Override
    public String toString() {
        return "ReservationDto{" +
                "id=" + id +
                ", cost=" + cost +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", comment='" + comment + '\'' +
                ", userId=" + userId +
                ", userUsername='" + userUsername + '\'' +
                ", lodgingOwnerId=" + lodgingOwnerId +
                ", lodgingOwnerUsername='" + lodgingOwnerUsername + '\'' +
                ", lodgingId=" + lodgingId +
                ", lodgingName=" + lodgingName +
                '}';
    }
}
