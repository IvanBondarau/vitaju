package by.bsuir.ivan_bondarau.vitaju.dto;

import by.bsuir.ivan_bondarau.vitaju.validation.PasswordMatches;
import by.bsuir.ivan_bondarau.vitaju.validation.ValidEmail;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@PasswordMatches
public class UserDto {

    private Long id;

    @NotNull
    @NotEmpty
    @Length(min = 5, max = 30)
    private String username;

    @ValidEmail
    @NotNull
    private String email;

    @NotNull
    @NotEmpty
    @Length(min = 5, max = 30)
    private String password;

    private String passwordConfirmation;

    @NotNull
    private BigDecimal balance;

    @Length(max = 50)
    private String fullName;

    @Length(max = 50)
    private String contactEmail;

    @Length(max = 50)
    private String phone;


    private String location;

    private Long userPersonalId;
    private Long userWalletId;

    @NotNull
    private Set<String> roles;

    public UserDto() {
        this.roles = new HashSet<>();
        this.username = "";
        this.email = "";
        this.password = "";
        this.balance = BigDecimal.ZERO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public Long getUserPersonalId() {
        return userPersonalId;
    }

    public void setUserPersonalId(Long userPersonalId) {
        this.userPersonalId = userPersonalId;
    }

    public Long getUserWalletId() {
        return userWalletId;
    }

    public void setUserWalletId(Long userWalletId) {
        this.userWalletId = userWalletId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto userDto = (UserDto) o;
        return Objects.equals(id, userDto.id) &&
                Objects.equals(username, userDto.username) &&
                Objects.equals(email, userDto.email) &&
                Objects.equals(password, userDto.password) &&
                Objects.equals(balance, userDto.balance) &&
                Objects.equals(fullName, userDto.fullName) &&
                Objects.equals(contactEmail, userDto.contactEmail) &&
                Objects.equals(phone, userDto.phone) &&
                Objects.equals(roles, userDto.roles);
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", passwordConfirmation='" + passwordConfirmation + '\'' +
                ", balance=" + balance +
                ", fullName='" + fullName + '\'' +
                ", contactEmail='" + contactEmail + '\'' +
                ", phone='" + phone + '\'' +
                ", roles=" + roles +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, email, password, balance, fullName, contactEmail, phone, roles);
    }
}
