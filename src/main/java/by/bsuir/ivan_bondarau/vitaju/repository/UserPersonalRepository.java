package by.bsuir.ivan_bondarau.vitaju.repository;

import by.bsuir.ivan_bondarau.vitaju.model.UserPersonal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserPersonalRepository extends JpaRepository<UserPersonal, Long> {

}
