package by.bsuir.ivan_bondarau.vitaju.repository;

import by.bsuir.ivan_bondarau.vitaju.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {
    Optional<UserAccount> findByUsername(String username);

    boolean existsByEmailAndActive(String email, boolean active);
    boolean existsByUsernameAndActive(String username, boolean active);
}
