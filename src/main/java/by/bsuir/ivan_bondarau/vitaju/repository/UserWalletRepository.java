package by.bsuir.ivan_bondarau.vitaju.repository;

import by.bsuir.ivan_bondarau.vitaju.model.UserWallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface UserWalletRepository extends JpaRepository<UserWallet, Long> {

}
