package by.bsuir.ivan_bondarau.vitaju.repository;

import by.bsuir.ivan_bondarau.vitaju.model.Lodging;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LodgingRepository extends JpaRepository<Lodging, Long> {
    List<Lodging> findByNameLike(String name);
    List<Lodging> findByLocationLike(String location);
    List<Lodging> findByNameLikeAndLocationLike(String name, String location);
}
