package by.bsuir.ivan_bondarau.vitaju.repository;

import by.bsuir.ivan_bondarau.vitaju.model.Facility;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FacilityRepository extends JpaRepository<Facility, Long> {
    Optional<Facility> findByName(String name);
}
