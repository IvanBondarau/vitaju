
CREATE TABLE public.facility (
                                 id bigint NOT NULL,
                                 name VARCHAR(255)
);



CREATE TABLE public.lodging (
                                id bigint NOT NULL,
                                cost numeric(19,2),
                                description VARCHAR(255),
                                image_name VARCHAR(255),
                                location VARCHAR(255),
                                name VARCHAR(255),
                                owner_id bigint
);


CREATE TABLE public.lodging_facilities (
                                           lodging_id bigint NOT NULL,
                                           facility_id bigint NOT NULL
);


CREATE TABLE public.reservation (
                                    id bigint NOT NULL,
                                    comment VARCHAR(255),
                                    cost numeric(19,2),
                                    end_date timestamp,
                                    start_date timestamp,
                                    lodging_id bigint,
                                    user_id bigint
);


CREATE TABLE public.user_account (
                                     id bigint NOT NULL,
                                     active boolean,
                                     email VARCHAR(255),
                                     password VARCHAR(255),
                                     username VARCHAR(255),
                                     user_contact_id bigint,
                                     user_wallet_id bigint
);


CREATE TABLE public.user_account_roles (
                                           user_account_id bigint NOT NULL,
                                           user_role_id bigint NOT NULL
);


CREATE TABLE public.user_personal (
                                      id bigint NOT NULL,
                                      contact_email VARCHAR(255),
                                      full_name VARCHAR(255),
                                      location VARCHAR(255),
                                      phone VARCHAR(255)
);


CREATE TABLE public.user_role (
                                  id bigint NOT NULL,
                                  name VARCHAR(255)
);


CREATE TABLE public.user_wallet (
                                    id bigint NOT NULL,
                                    balance numeric(19,2)
);


CREATE SEQUENCE public.vitaju_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.facility
    ADD CONSTRAINT facility_pkey PRIMARY KEY (id);



ALTER TABLE public.lodging_facilities
    ADD CONSTRAINT lodging_facilities_pkey PRIMARY KEY (lodging_id, facility_id);

ALTER TABLE public.lodging
    ADD CONSTRAINT lodging_pkey PRIMARY KEY (id);


ALTER TABLE public.reservation
    ADD CONSTRAINT reservation_pkey PRIMARY KEY (id);


ALTER TABLE public.user_account
    ADD CONSTRAINT user_account_pkey PRIMARY KEY (id);

ALTER TABLE public.user_account_roles
    ADD CONSTRAINT user_account_roles_pkey PRIMARY KEY (user_account_id, user_role_id);


ALTER TABLE public.user_personal
    ADD CONSTRAINT user_personal_pkey PRIMARY KEY (id);


ALTER TABLE public.user_role
    ADD CONSTRAINT user_role_pkey PRIMARY KEY (id);

ALTER TABLE public.user_wallet
    ADD CONSTRAINT user_wallet_pkey PRIMARY KEY (id);

ALTER TABLE public.lodging
    ADD CONSTRAINT fkanouweq5b98kby8tbwnhfowyl FOREIGN KEY (owner_id) REFERENCES public.user_account(id);


ALTER TABLE public.lodging_facilities
    ADD CONSTRAINT fkb92l2io2voupe2in04mm2jp24 FOREIGN KEY (facility_id) REFERENCES public.facility(id);


ALTER TABLE public.reservation
    ADD CONSTRAINT fkcinbsb1axs6c5sh84ay3qixaq FOREIGN KEY (user_id) REFERENCES public.user_account(id);


ALTER TABLE public.lodging_facilities
    ADD CONSTRAINT fkdoknuhaih8h7w2dk67xc83t5s FOREIGN KEY (lodging_id) REFERENCES public.lodging(id);


ALTER TABLE public.reservation
    ADD CONSTRAINT fkl68gvyt1unaknsrppch6kaddo FOREIGN KEY (lodging_id) REFERENCES public.lodging(id);


ALTER TABLE public.user_account
    ADD CONSTRAINT fkm1qg8pmhtoewo3xohq06pvkwl FOREIGN KEY (user_contact_id) REFERENCES public.user_personal(id);

ALTER TABLE public.user_account
    ADD CONSTRAINT fkmxpcuecwar0aw05suwg1ihqip FOREIGN KEY (user_wallet_id) REFERENCES public.user_wallet(id);

ALTER TABLE public.user_account_roles
    ADD CONSTRAINT fkoebehw60c7m70x68j3caq0st3 FOREIGN KEY (user_role_id) REFERENCES public.user_role(id);

ALTER TABLE public.user_account_roles
    ADD CONSTRAINT fkpacca51k3kkqoqs0nbmyugdt2 FOREIGN KEY (user_account_id) REFERENCES public.user_account(id);
