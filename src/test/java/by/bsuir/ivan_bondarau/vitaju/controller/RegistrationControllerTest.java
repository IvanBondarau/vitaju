package by.bsuir.ivan_bondarau.vitaju.controller;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RegistrationControllerTest {

    private static final String TOKEN_ATTR_NAME = "org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository.CSRF_TOKEN";
    @Autowired
    private MockMvc mockMvc;

    public RegistrationControllerTest() {
    }

    @Test
    public void testRegisterPage() throws Exception {
        System.out.println(mockMvc);
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/register")
        )
                .andExpect(status().isOk())
                .andExpect(view().name("registration"))
                .andReturn();
    }


    @Test
    public void testRegisterSuccessful() throws Exception {
        HttpSessionCsrfTokenRepository httpSessionCsrfTokenRepository = new HttpSessionCsrfTokenRepository();
        CsrfToken csrfToken = httpSessionCsrfTokenRepository
                .generateToken(new MockHttpServletRequest());

        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.post("/register")
                        .sessionAttr(TOKEN_ATTR_NAME, csrfToken)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .content(EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
                                new BasicNameValuePair("username", "trrrrrBUSsKAALLAAAA"),
                                new BasicNameValuePair("password", "bigTestPassword"),
                                new BasicNameValuePair("passwordConfirmation", "bigTestPassword"),
                                new BasicNameValuePair("email", "tesdadsStVLAqDIMIR@email.com"),
                                new BasicNameValuePair(csrfToken.getParameterName(),  csrfToken.getToken())
                        ))))
        )
                .andDo(print())
                .andExpect(status().isFound())
                .andReturn();
    }

}