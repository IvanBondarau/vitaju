package by.bsuir.ivan_bondarau.vitaju.service

import by.bsuir.ivan_bondarau.vitaju.converter.LodgingConverter
import by.bsuir.ivan_bondarau.vitaju.dto.LodgingDto
import by.bsuir.ivan_bondarau.vitaju.dto.LodgingSearchCriteria
import by.bsuir.ivan_bondarau.vitaju.exception.InvalidLodgingOwnerException
import by.bsuir.ivan_bondarau.vitaju.exception.LodgingNotFoundException
import by.bsuir.ivan_bondarau.vitaju.model.Facility
import by.bsuir.ivan_bondarau.vitaju.model.Lodging
import by.bsuir.ivan_bondarau.vitaju.model.UserAccount
import by.bsuir.ivan_bondarau.vitaju.repository.FacilityRepository
import by.bsuir.ivan_bondarau.vitaju.repository.LodgingRepository
import by.bsuir.ivan_bondarau.vitaju.repository.ReservationRepository
import by.bsuir.ivan_bondarau.vitaju.repository.UserAccountRepository
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.util.*
import javax.persistence.EntityNotFoundException

@RunWith(MockitoJUnitRunner::class)
class LodgingServiceImplTest {
    @Mock
    lateinit var accountRepository: UserAccountRepository
    @Mock
    lateinit var lodgingRepository: LodgingRepository
    @Mock
    lateinit var facilityRepository: FacilityRepository
    @Mock
    lateinit var reservationRepository: ReservationRepository
    @Mock
    lateinit var lodgingConverter: LodgingConverter

    @InjectMocks
    lateinit var lodgingService: LodgingServiceImpl


    @Test
    fun testCreateOk() {

        val testLodging = Lodging()
        testLodging.name = "Test"
        val lodgingOwner = UserAccount()
        lodgingOwner.id = 10
        testLodging.owner = lodgingOwner
        testLodging.facilities = setOf(Facility("aba"))
        val testFacility = Facility("aba")
        testFacility.id = 1

        whenever(facilityRepository.findByName(any())).thenReturn(Optional.of(testFacility))
        whenever(accountRepository.getOne(any())).thenReturn(lodgingOwner)
        whenever(lodgingConverter.convertToEntity(any())).thenReturn(testLodging)
        lodgingService.create(LodgingDto())

        Mockito.verify(lodgingRepository).save(testLodging)
    }

    @Test(expected = InvalidLodgingOwnerException::class)
    fun testCreateNoOwner() {
        val testLodging = Lodging()
        testLodging.name = "Test"

        testLodging.facilities = setOf()

        whenever(lodgingConverter.convertToEntity(any())).thenReturn(testLodging)
        lodgingService.create(LodgingDto())
    }

    @Test
    fun testReadOk() {
        val testLodging = Lodging()
        testLodging.name = "Test"
        testLodging.id = 1

        whenever(lodgingRepository.getOne(1)).thenReturn(testLodging)

        lodgingService.read(1)

        verify(lodgingRepository).getOne(1)
        verify(lodgingConverter).convertToDto(testLodging)

    }

    @Test(expected = EntityNotFoundException::class)
    fun testReadNotFound() {

        whenever(lodgingRepository.getOne(1)).thenThrow(EntityNotFoundException::class.java)

        lodgingService.read(1)
    }

    @Test
    fun testUpdateOk() {
        val testLodging = Lodging()
        testLodging.name = "Test"
        val lodgingOwner = UserAccount()
        lodgingOwner.id = 10
        testLodging.owner = lodgingOwner
        testLodging.facilities = setOf()

        whenever(accountRepository.getOne(any())).thenReturn(lodgingOwner)
        whenever(lodgingConverter.convertToEntity(any())).thenReturn(testLodging)
        lodgingService.update(LodgingDto())

        Mockito.verify(lodgingRepository).save(testLodging)
    }

    @Test
    fun testDeleteOk() {
        whenever(lodgingRepository.existsById(any())).thenReturn(true)
        lodgingService.delete(1)
        verify(reservationRepository).deleteByLodgingId(1)
        verify(lodgingRepository).deleteById(1)
    }

    @Test(expected = LodgingNotFoundException::class)
    fun testDeleteNotFound() {
        whenever(lodgingRepository.existsById(any())).thenReturn(false)
        lodgingService.delete(1)
    }

    @Test
    fun testSearchAllParams() {
        val searchCriteria = LodgingSearchCriteria()
        searchCriteria.name = "aba"
        searchCriteria.location = "test"

        whenever(lodgingRepository.findByNameLikeAndLocationLike(any(), any()))
                .thenReturn(listOf())
        lodgingService.find(searchCriteria)

        verify(lodgingRepository).findByNameLikeAndLocationLike("%aba%", "%test%")
    }

    @Test
    fun testSearchOneParam() {
        val searchCriteria = LodgingSearchCriteria()
        searchCriteria.name = "aba"

        whenever(lodgingRepository.findByNameLike(any()))
                .thenReturn(listOf())
        lodgingService.find(searchCriteria)

        verify(lodgingRepository).findByNameLike("%aba%")
    }

    @Test
    fun testFindAllOk() {
        whenever(lodgingRepository.findAll()).thenReturn(listOf())
        lodgingService.findAll()
        verify(lodgingRepository).findAll()
    }
}